package com.example.td;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
 private TextView tvResultAddition,tvResultSoustraction, tvResultProduit, tvResultDivision;
 private EditText etGauche, etDroit;
 private static final int DIALOG_ALERT = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        etGauche = (EditText) findViewById(R.id.editText1);
        etDroit = (EditText) findViewById(R.id.editText2);
        tvResultAddition = (TextView) findViewById(R.id.resultAddition);
        tvResultSoustraction = (TextView) findViewById(R.id.resultSoustraction);
        tvResultProduit = (TextView) findViewById(R.id.resultProduit);
        tvResultDivision = (TextView) findViewById(R.id.resultDivision);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showDialog(DIALOG_ALERT);
        }

        return super.onOptionsItemSelected(item);
    }


    public void computeAdd() {
        int add =  Integer.parseInt(this.etDroit.getText().toString()) + Integer.parseInt( this.etGauche.getText().toString());
        this.tvResultAddition.setText(String.valueOf(add));
    }

    public void computeSous() {
        int soust = Math.abs(Integer.parseInt(this.etDroit.getText().toString()) - Integer.parseInt( this.etGauche.getText().toString()));
        this.tvResultSoustraction.setText(String.valueOf(soust));
    }

    public void computeProd() {
        int prod = Integer.parseInt(this.etDroit.getText().toString()) * Integer.parseInt( this.etGauche.getText().toString());
        this.tvResultProduit.setText(String.valueOf(prod));

    }

    public void computeDivision() {
        int div = 0;
        if(!this.etDroit.getText().toString().equals("0"))
            div = Integer.parseInt(this.etGauche.getText().toString()) / Integer.parseInt(this.etDroit.getText().toString());
        this.tvResultDivision.setText(String.valueOf(div));
    }

    public void computeAll(View v) {
        this.computeAdd();
        this.computeSous();
        this.computeProd();
        this.computeDivision();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("TD1");
                builder.setMessage(Html.fromHtml("<p>L'application a été dévelopée par <b>Gauthier Vangrevelynghe</b>.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new MainActivity.OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }
}
