package com.example.td;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class CalculatriceActivity extends AppCompatActivity {
    private TextView result;
    private static final int DIALOG_ALERT = 10;
    private static String currentEntered = "";
    private static String currentOperator = "";
    private static Boolean hasEntered = false;
    private final String CONSTANT_ADD = "+", CONSTANT_SOUST = "-", CONSTANT_PROD = "x", CONSTANT_DIV = "/", CONSTANT_ENTER = "=", CONSTANT_CLEAR = "c", CONSTANT_ERASE = "e";
    private final ArrayList<String> operators = new ArrayList<String>(Arrays.asList(CONSTANT_ADD, CONSTANT_DIV, CONSTANT_SOUST, CONSTANT_PROD));
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculatrice);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        result = (TextView) findViewById(R.id.result);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showDialog(DIALOG_ALERT);
        }

        return super.onOptionsItemSelected(item);
    }

    public void enterNumber(View v){
       switch (v.getId()) {
           case R.id.enter0: updateTxtView("0");break;
           case R.id.enter1: updateTxtView("1"); break;
           case R.id.enter2: updateTxtView("2");break;
           case R.id.enter3: updateTxtView("3");break;
           case R.id.enter4: updateTxtView("4");break;
           case R.id.enter5: updateTxtView("5");break;
           case R.id.enter6: updateTxtView("6");break;
           case R.id.enter7: updateTxtView("7");break;
           case R.id.enter8: updateTxtView("8");break;
           case R.id.enter9: updateTxtView("9");break;
           case R.id.addition: updateTxtView(CONSTANT_ADD);break;
           case R.id.produit: updateTxtView(CONSTANT_PROD);break;
           case R.id.soustraction: updateTxtView(CONSTANT_SOUST);break;
           case R.id.division: updateTxtView(CONSTANT_DIV);break;
           case R.id.btnEnter: updateTxtView(CONSTANT_ENTER);break;
           case R.id.erase: updateTxtView(CONSTANT_ERASE);break;
           case R.id.reset: updateTxtView(CONSTANT_CLEAR);break;
       }
    }

    public void updateTxtView(String entered) {


        if(entered.equals(CONSTANT_CLEAR)) { //CLEAR
            result.setText("0");
            currentOperator = ""; currentEntered = "";
        }
        else if(entered.equals(CONSTANT_ERASE)) { //EFFACER
            if(!result.getText().toString().equals("0")) {
                String toSubstring = result.getText().toString();
                if(toSubstring.length() > 1)
                    toSubstring = toSubstring.substring(0, toSubstring.length()-1);
                else toSubstring = "0";
                result.setText(toSubstring);
            }
        }
        else if(entered.equals(CONSTANT_ENTER)) { //Calculer et afficher le résultat
            if(!operators.contains(result.getText().toString()) && !currentOperator.equals("")) {
                int number1 = Integer.parseInt(currentEntered);
                int number2 = Integer.parseInt(result.getText().toString());
                int resultat = 0;
                switch (currentOperator) {
                    case CONSTANT_ADD: resultat = computeAdd(number1, number2);break;
                    case CONSTANT_PROD: resultat = computeProd(number1, number2);break;
                    case CONSTANT_SOUST: resultat = computeSous(number1, number2);break;
                    case CONSTANT_DIV: resultat = computeDivision(number1, number2);break;
                }
                result.setText(String.valueOf(resultat));
                currentOperator = "";
            }
        }
        else if(operators.contains(entered) || result.getText().toString().equals("") || result.getText().toString().equals("0")) {
            //Si appui sur un opérateur
            if(operators.contains(entered)) {
                if (!operators.contains(result.getText().toString()))
                    currentEntered = result.getText().toString(); //Sauvegarder le premier nombre

                //Sauvegarder l'opérateur
                currentOperator = entered;
            }
            result.setText(entered);
        }
        else { //Chiffre entré
            if(operators.contains(result.getText().toString())) {
                result.setText(entered);
            }
            else if(!result.getText().toString().equals("0")){
                result.setText(result.getText().toString() + "" + entered);
            }

        }
    }

    public int computeAdd(int n1, int n2) {
        return n1+n2;
    }

    public int computeSous(int n1, int n2) {
        if(n1 >= n2) return n1-n2;
        else return 0;
    }

    public int computeProd(int n1, int n2) {
        return n1*n2;

    }

    public int computeDivision(int n1, int n2) {
        if(n2 != 0) {
            return n1/n2;
        }
        else return 0;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("TD1");
                builder.setMessage(Html.fromHtml("<p>L'application a été dévelopée par <b>Gauthier Vangrevelynghe</b>."));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new CalculatriceActivity.OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }

}
