package com.example.gvangrevelynghe.td4;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class RepoListAdapter extends RecyclerView.Adapter<RepoListViewHolder>{

    private List<Repo> listRepos;

    public RepoListAdapter(List<Repo> listRepo) {this.listRepos = listRepo;}

    @Override
    public RepoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
        return new RepoListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RepoListViewHolder holder, int position) {
        holder.display(listRepos.get(position));
    }

    @Override
    public int getItemCount() {
        return listRepos.size();
    }
}
