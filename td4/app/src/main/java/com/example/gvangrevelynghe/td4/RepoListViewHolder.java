package com.example.gvangrevelynghe.td4;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class RepoListViewHolder extends RecyclerView.ViewHolder {

    private TextView idView, nameView;
    public RepoListViewHolder(View itemView) {
        super(itemView);

        idView = itemView.findViewById(R.id.idRepo);
        nameView = itemView.findViewById(R.id.nameRepo);
    }

    public void display(Repo repo) {
        idView.setText(repo.getId().toString());
        nameView.setText(repo.getName());
    }
}
