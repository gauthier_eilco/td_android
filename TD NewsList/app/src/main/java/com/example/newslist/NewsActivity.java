package com.example.newslist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.text.Html;
import android.util.EventLog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NewsActivity extends AppCompatActivity {
    private static final int DIALOG_ALERT = 10;
    NewsListApplication app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        app = (NewsListApplication) getApplicationContext();
        Button button_logout = findViewById(R.id.button_logout);
        Button button_details = findViewById(R.id.button_detail);
        String login = app.getLogin();
        TextView txt = (TextView) findViewById(R.id.viewLogin);
        txt.setText("Bienvenue " + login);

        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsActivity.this, LoginActivity.class));
            }
        });

        button_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsActivity.this, DetailsActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void openWebPage(View v) {
        String url = "http://android.busin.fr/";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.settings) {
            showDialog(DIALOG_ALERT);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("TD2");
                builder.setMessage(Html.fromHtml("<p>L'application a été dévelopée par <b>Gauthier Vangrevelynghe</b>.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new NewsActivity.OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }
}
