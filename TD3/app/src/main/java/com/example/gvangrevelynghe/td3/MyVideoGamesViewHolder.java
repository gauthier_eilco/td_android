package com.example.gvangrevelynghe.td3;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView.ViewHolder;

public class MyVideoGamesViewHolder extends ViewHolder {
    private TextView priceView, nameView;
    public MyVideoGamesViewHolder(View itemView) {
        super(itemView);

        priceView = itemView.findViewById(R.id.price);
        nameView = itemView.findViewById(R.id.name);
    }

    public void display(JeuVideo jeuVideo) {
        nameView.setText(jeuVideo.getName());
        priceView.setText(String.valueOf(jeuVideo.getPrice()) +"$");
    }
}
