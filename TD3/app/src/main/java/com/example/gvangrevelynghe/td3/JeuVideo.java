package com.example.gvangrevelynghe.td3;

public class JeuVideo {

    private String name;
    private float price;

    public JeuVideo(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public String getName(){

        return this.name;
    }

    public float getPrice(){

        return this.price;
    }

    public void setName(String name){

        this.name = name;
    }

    public void setPrice(){

        this.price = price;
    }
}
