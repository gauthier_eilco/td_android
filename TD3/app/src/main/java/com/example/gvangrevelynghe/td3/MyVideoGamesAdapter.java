package com.example.gvangrevelynghe.td3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import java.util.*;

public class MyVideoGamesAdapter extends RecyclerView.Adapter<MyVideoGamesViewHolder>{
    private List<JeuVideo> mesJeux;

    public MyVideoGamesAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }

    @Override
    public MyVideoGamesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video, parent, false);
        return new MyVideoGamesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyVideoGamesViewHolder holder, int position) {
        holder.display(mesJeux.get(position));
    }

    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}
