package com.example.project.Interface.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Interface.ViewHolder.CreditListViewHolder;
import com.example.project.Interface.ViewHolder.FilmListViewHolder;
import com.example.project.Model.Cast;
import com.example.project.Model.Film;
import com.example.project.R;

public class CreditListAdapter extends RecyclerView.Adapter<CreditListViewHolder>{

    private List<Cast> casts;

    public CreditListAdapter(List<Cast> castToDisplay) {this.casts = castToDisplay;}

    @Override
    public CreditListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_film_cast, parent, false);
        return new CreditListViewHolder(view, casts);
    }

    @Override
    public void onBindViewHolder(CreditListViewHolder holder, int position) {
        holder.display(casts.get(position));
    }

    @Override
    public int getItemCount() {
        return casts.size();
    }
}