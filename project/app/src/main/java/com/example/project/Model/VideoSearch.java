package com.example.project.Model;

import java.util.List;

public class VideoSearch {

    private List<Video> results;

    public List<Video> getResults() {
        return results;
    }

    public void setResults(List<Video> results) {
        this.results = results;
    }
}
