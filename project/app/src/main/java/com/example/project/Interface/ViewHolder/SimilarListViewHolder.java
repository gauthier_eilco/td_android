package com.example.project.Interface.ViewHolder;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Interface.Activity.DetailsActivity;
import com.example.project.Model.Cast;
import com.example.project.Model.Film;
import com.example.project.R;
import com.example.project.Service.DownloaderService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SimilarListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ImageView image;
    private List<Film> similars;

    public SimilarListViewHolder(View itemView, List<Film> similars) {
        super(itemView);
        this.similars = similars;
        itemView.setOnClickListener(this);
        image = itemView.findViewById(R.id.imageSimilar);
    }

    public void display(Film similar) {
        if(similar.getPosterPath() != null && !similar.getPosterPath().isEmpty()) { //Si image
            new DownloaderService(image.getContext(), similar.getPosterPath(), image, 400,700);
        }
        else { //Si pas d'image, image par défaut
            new DownloaderService(image.getContext(), R.drawable.image_not_found, image, 400,700);
        }
    }

    @Override
    public void onClick(View v) {
        //Au click sur un item, on ouvre les détails du film
        Film clicked = similars.get(getLayoutPosition());
        Intent details = new Intent(v.getContext() , DetailsActivity.class);
        details.putExtra("FilmId", clicked.getId().toString());
        v.getContext().startActivity(details);

    }

}

