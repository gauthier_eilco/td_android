package com.example.project.Interface.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.project.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class SettingsActivity extends AppCompatActivity {
    private Button toggleFrenchBtn, toggleEnglishBtn, toggleSweetBtn, toggleDarkBtn, aboutBtn;
    private EditText sizeTextEdt;
    private BottomNavigationView bottomNavigationView;
    private static final int DIALOG_ALERT = 10;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private final static String SETTINGS = "settings";
    private final static String LANGUAGE = "language";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toggleEnglishBtn = findViewById(R.id.toggleEnglish);
        toggleFrenchBtn = findViewById(R.id.toggleFrench);
        toggleDarkBtn = findViewById(R.id.toggleDarkTheme);
        toggleSweetBtn = findViewById(R.id.toggleSweetTheme);
        sizeTextEdt = (EditText) findViewById(R.id.editTextSizeText);
        aboutBtn = findViewById(R.id.AboutBtn);

        sharedpreferences = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);
        String language = sharedpreferences.getString(LANGUAGE, "Original");
        editor = sharedpreferences.edit();

        if(!language.equals("Original")) {
            toggleFrenchBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleEnglishBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleEnglishBtn); addIcon(toggleFrenchBtn);
            editor.putString(LANGUAGE, "French");
        }
        else {
            toggleEnglishBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleFrenchBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleFrenchBtn); addIcon(toggleEnglishBtn);
            editor.putString(LANGUAGE, "Original");
        }
        editor.commit();

        //Modification du header de la page
        getSupportActionBar().setTitle("Préférences");
        toolbar.setSubtitle("Modifiez les paramètres de l'application");
        toolbar.setLogo(android.R.drawable.ic_menu_preferences);

        //Configuration du menu de navigation du bas
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_main_bottom_navigation);
        this.configureBottomView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       /* //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


    public void setLanguage(View view) {

        editor = sharedpreferences.edit();
        if(R.id.toggleEnglish == view.getId()) {
            toggleEnglishBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleFrenchBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleFrenchBtn); addIcon(toggleEnglishBtn);
            editor.putString(LANGUAGE, "Original");
        }
        else {
            toggleFrenchBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleEnglishBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleEnglishBtn); addIcon(toggleFrenchBtn);
            editor.putString(LANGUAGE, "French");
        }

        editor.commit();


    }

    public void setTheme(View view) {
        if(R.id.toggleSweetTheme == view.getId()) {
            toggleSweetBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleDarkBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleDarkBtn); addIcon(toggleSweetBtn);
        }
        else {
            toggleDarkBtn.setBackgroundColor(Color.parseColor("#00ba1d"));
            toggleSweetBtn.setBackgroundColor(Color.parseColor("#D6D6D6"));
            removeIcon(toggleSweetBtn); addIcon(toggleDarkBtn);
        }
    }

    public void removeIcon(Button btn) {
        btn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0 );
    }

    public void addIcon(Button btn) {
        btn.setCompoundDrawablesWithIntrinsicBounds(android.R.drawable.ic_menu_save, 0, 0, 0 );
    }

    public void onClick(View view) {
        showDialog(DIALOG_ALERT);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("MoviMee");
                builder.setMessage(Html.fromHtml("<p>L'application MoviMee a été dévelopée en 2019 par <b>Gauthier Vangrevelynghe</b> et <b>Jori Mathurin</b> dans le cadre d'un projet d'école.</p><p>Elle vous permet de facilement naviguer au sein d'une large collection de films populaires et d'en obtenir les détails.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new SettingsActivity.OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }

    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_menu_accueil:
                        intent = new Intent(SettingsActivity.this.getApplicationContext(), MainActivity.class);
                        break;
                    case R.id.action_menu_search:
                        intent = new Intent(SettingsActivity.this.getApplicationContext(), SearchActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_settings:
                        intent = new Intent(SettingsActivity.this.getApplicationContext(), SettingsActivity.class);
                        break;
                }
                startActivity(intent);
                return true;
            }
        });
        bottomNavigationView.getMenu().findItem(R.id.action_menu_settings).setChecked(true);
    }
}
