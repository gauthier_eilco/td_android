package com.example.project.Model;

import java.util.ArrayList;
import java.util.List;

public class ObjectVideo {
    int id;
    List<ResultsVideo> results = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public  List<ResultsVideo> getResults() {
        return results;
    }

    public void setResults(List<ResultsVideo> results) {
        this.results = results;
    }
}

