package com.example.project.Service;

import android.graphics.Color;

public class UtilService {
    private static String[] months = new String[]{
        "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
    };
    private static String[] monthsUK = new String[]{
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    };
    private final static int color_green = Color.rgb(0,128,0);
    private final static int color_yellow = Color.rgb(235,232,52);
    private static final String FR = "Français", FR_API = "French";
    private static final String UK = "Anglais", UK_API = "English";

    public UtilService() {}

    public static String formaterDate(String date) {
        String[] split = date.split("-");
        return split[2] + " " + months[Integer.valueOf(split[1])-1] + " " + split[0];
    }

    public static String formaterDateUK(String date) {
        String[] split = date.split("-");
        return monthsUK[Integer.valueOf(split[1])-1] + " " + split[2] + " " + split[0];
    }

    public static int getColorPopularity(float popularity) {
        if(popularity > 25) {
            return color_green;
        }
        else if(popularity > 10) {
            return color_yellow;
        }
        else return Color.RED;
    }

    public static String getTextPopularity(int colorRGB, String langue) {
        if(langue.equals(UK)) {
            if(colorRGB == color_green) return "High popularity";
            else if (colorRGB == color_yellow) return "Medium popularity";
            else return "Low popularity";
        }
        else {
            if(colorRGB == color_green) return "Popularité élevée";
            else if (colorRGB == color_yellow) return "Popularité moyenne";
            else return "Popularité faible";
        }

    }

}
