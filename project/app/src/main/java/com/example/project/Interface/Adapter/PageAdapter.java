package com.example.project.Interface.Adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.project.Model.Film;

import java.io.IOException;
import java.util.List;


public class PageAdapter extends FragmentPagerAdapter {



    private List<Film> resultsList;


    public PageAdapter(FragmentManager mgr, List<Film> resultsList)
    {
        super(mgr);
        this.resultsList = resultsList;

    }

    @Override
    public int getCount() {
        return(resultsList.size());
    }

    @Override
    public Fragment getItem(int position)
    {
        PageFragment p =new PageFragment();

        try {
            return(PageFragment.newInstance(position, resultsList.get(position)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }


    public Film getresults (int position)
    {
        return resultsList.get(position);

    }



}




