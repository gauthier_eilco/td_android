package com.example.project.Controller;

import com.example.project.Model.Credits;
import com.example.project.Model.Film;
import com.example.project.Model.Genre;
import com.example.project.Model.GenreSearch;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.ResultSearch;
import com.example.project.Model.ResultTranslationsRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TmdbClient {

    @GET("search/movie")
    Call<ResultSearch> getFilmsBySearch(@Query("query") String term, @Query("api_key") String api_key, @Query("page") int page);

    @GET("movie/{movie_id}")
    Call<Film> getFilmById(@Path("movie_id") String movie_id, @Query("api_key") String api_key);

    @GET("genre/movie/list")
    Call<GenreSearch> getGenresFr(@Query("api_key") String api_key, @Query("language") String lang);

    @GET("movie/{movie_id}/credits")
    Call<Credits> getCreditsByFilm(@Path("movie_id") String movie_id, @Query("api_key") String api_key);

    @GET("movie/{movie_id}/similar")
    Call<ResultSearch> getSimilarFilms(@Path("movie_id") String movie_id, @Query("api_key") String api_key);

    @GET("movie/{movie_id}/translations")
    Call<ResultTranslationsRequest> getTranslationsOfFilm(@Path("movie_id") String movie_id, @Query("api_key") String api_key);

    @GET("popular?")
    Call<ResultSearch> getfilmpopLanguageandPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);

    @GET("list?")
    Call<ResultSearch> getgenresLanguageandPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);

    @GET("movie?")
    Call<ResultSearch> getfilmsgenresLanguageandPage(@Query("api_key") String api_key, @Query("with_genres")int with_genres,@Query("language") String language, @Query("page") int page);

    @GET("videos?")
    Call<ObjectVideo> getLanguage(@Query("api_key") String api_key, @Query("language") String language);

    @GET("discover/movie")
    Call<ResultSearch> getFilmsByGenre(@Query("api_key") String api_key, @Query("language") String language, @Query("with_genres") String id_genre);
    @GET("now_playing?")
    Call<ResultSearch> getnow_playingLanguageandPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);
    @GET("top_rated?")
    Call<ResultSearch> gettop_ratedLanguageandPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);
    @GET("upcoming?")
    Call<ResultSearch> getupcomingLanguageandPage(@Query("api_key") String api_key, @Query("language") String language, @Query("page") int page);

}