package com.example.project.Interface.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.SearchView;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.Adapter.FilmListAdapter;
import com.example.project.Model.Film;
import com.example.project.Model.ResultSearch;
import com.example.project.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private TextView textViewResultInfo;
    private SearchView searchView;
    private Spinner filterSelect;
    private Retrofit.Builder builder;
    private TmdbClient client;
    private static final int DIALOG_ALERT = 10;
    private RecyclerView recyclerView;
    private String searchText;
    private int current_page = 1, total_page;
    private List<Film> movies  = new ArrayList<Film>();
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private final static String SETTINGS = "settings";
    private final static String LANGUAGE = "language";
    private static final String API_KEY = "a870ad0110ff6bafa39b352d0dd503dd";
    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String FILTER_POPULAIRE = "Plus populaire";
    private static final String FILTER_DATE = "Plus récent";
    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sharedpreferences = getSharedPreferences(SETTINGS, Context.MODE_PRIVATE);

        language = sharedpreferences.getString(LANGUAGE, "Original");

        //Menu de filtrage
        filterSelect = findViewById(R.id.filterSelect);
        filterSelect.setAdapter(new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, new String[]{FILTER_POPULAIRE, FILTER_DATE}));
        filterSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selected = filterSelect.getAdapter().getItem(position).toString();
                switch (selected) {
                    case FILTER_DATE: filterByDate(); break;
                    case FILTER_POPULAIRE: filterByPopularity(); break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Modification du header de la page
        getSupportActionBar().setTitle("Recherche");
        toolbar.setSubtitle("Recherchez les films qui vous font envie");
        toolbar.setLogo(android.R.drawable.ic_menu_search);

        //Configuration du menu de navigation du bas
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_main_bottom_navigation);
        this.configureBottomView();

        //initialisation du builder pour le call à l'API
        builder = new Retrofit.Builder()
                .baseUrl( BASE_URL )
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        client = retrofit.create(TmdbClient.class);


        //Récupération de la searchView et paramétrages
        searchView = findViewById(R.id.mainSearchView);
        textViewResultInfo = findViewById(R.id.textViewResultInfo);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(newText.length() > 0 && !newText.isEmpty())
                {
                    searchText = newText;
                    current_page = 1;
                    Call<ResultSearch> call = client.getFilmsBySearch(newText, API_KEY, current_page);

                    call.enqueue(new Callback<ResultSearch>() {
                        @Override
                        public void onResponse(Call<ResultSearch> call, Response<ResultSearch> response) {
                            movies = new ArrayList<Film>();
                            movies = response.body().getListFilms();
                            total_page = response.body().getTotalPages();
                            textViewResultInfo.setText(response.body().getTotalResults() + " résultats trouvés pour la recherche : " + searchText);
                            recyclerView = findViewById(R.id.RecylerViewListeFilmSearch);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                            recyclerView.setAdapter(new FilmListAdapter(movies, language));

                            initScrollListener();
                            //filterByPopularity();
                        }
                        @Override
                        public void onFailure(Call<ResultSearch> call, Throwable t) {
                            Log.d("call api", t.getMessage());
                            Toast.makeText(SearchActivity.this, "Error...!!! ", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else {
                    recyclerView = findViewById(R.id.RecylerViewListeFilmSearch);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setAdapter(null);
                    textViewResultInfo.setText("");
                }
                return false;
            }
        });
    }

    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_menu_accueil:
                        intent = new Intent(SearchActivity.this.getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_search:
                        intent = new Intent(SearchActivity.this.getApplicationContext(), SearchActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_settings:
                        showDialog(DIALOG_ALERT);
                        break;
                }

                return true;
            }
        });
        bottomNavigationView.getMenu().findItem(R.id.action_menu_search).setChecked(true);
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                //Détecter la fin de la première liste et charger la page suivante
                if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == recyclerView.getAdapter().getItemCount() - 5) {
                    current_page++;
                    if(current_page <= total_page) {
                        loadMore();
                        Log.d("taille", String.valueOf(movies.size()));
                    }
                }

            }
        });


    }

    private void loadMore() {
        Call<ResultSearch> call = client.getFilmsBySearch(searchText, API_KEY, current_page);

        call.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(Call<ResultSearch> call, Response<ResultSearch> response) {
                final List<Film> liste;
                liste = response.body().getListFilms();

                Log.d("taille", String.valueOf(movies.size()));

                //Ajouter les nouveaux films à la liste actuelle avec un délai
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int scrollPosition = movies.size();
                        recyclerView.getAdapter().notifyItemRemoved(scrollPosition);
                        movies.addAll(liste);
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }, 100);
            }

            @Override
            public void onFailure(Call<ResultSearch> call, Throwable t) {
                Log.d("call api", t.getMessage());
                Toast.makeText(SearchActivity.this, "Error...!!! ", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void filterByDate() {
        Collections.sort(movies, new Comparator<Film>() {
            @Override
            public int compare(Film lhs, Film rhs) {
                if(lhs.getReleaseDate() == null || lhs.getReleaseDate().isEmpty()) {
                    if(rhs.getReleaseDate() == null || rhs.getReleaseDate().isEmpty()) {
                        return 0;
                    }
                    else
                        return -1;
                }
                else if(rhs.getReleaseDate() == null || rhs.getReleaseDate().isEmpty()) {
                    return 1;
                }
                else {
                    Calendar sdf = new SimpleDateFormat("yyyy-MM-dd").getCalendar();
                    String[] split = lhs.getReleaseDate().split("-");
                    sdf.set(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
                    Date date1 = sdf.getTime();

                    split = rhs.getReleaseDate().split("-");
                    sdf.set(Integer.parseInt(split[0]), Integer.parseInt(split[1]), Integer.parseInt(split[2]));
                    Date date2 = sdf.getTime();

                    return (date1.compareTo(date2) >= 0) ? -1 : (date1.compareTo(date2) < 0) ? 1 : 0;
                }
            }
        });
        if(recyclerView != null) recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this, android.app.AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("MoviMee");
                builder.setMessage(Html.fromHtml("<p>L'application MoviMee a été dévelopée en 2019 par <b>Gauthier Vangrevelynghe</b> et <b>Jori Mathurin</b> dans le cadre d'un projet d'école.</p><p>Elle vous permet de facilement naviguer au sein d'une large collection de films populaires et d'en obtenir les détails.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new SearchActivity.OkOnClickListener());
                android.app.AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }

    private void filterByPopularity(){
        Collections.sort(movies, new Comparator<Film>() {
            @Override
            public int compare(Film o1, Film o2) {
                return (o1.getPopularity() > o2.getPopularity()) ? -1 : (o1.getPopularity() < o2.getPopularity()) ? 1 : 0;
            }
        });
        if(recyclerView != null) recyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        return;
    }


}
