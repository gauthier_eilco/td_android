package com.example.project.Interface.ViewHolder;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.Activity.DetailsActivity;
import com.example.project.Interface.Activity.SearchActivity;
import com.example.project.Model.Film;
import com.example.project.Model.ResultSearch;
import com.example.project.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilmListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView nameView, dateView, descriptionView;
    private ImageView image;
    private List<Film> liste;
    private static final String API_KEY = "a870ad0110ff6bafa39b352d0dd503dd";
    private View clikedView;

    public FilmListViewHolder(View itemView, List<Film> liste) {
        super(itemView);
        itemView.setOnClickListener(this);
        nameView = itemView.findViewById(R.id.nameFilmSearch);
        dateView = itemView.findViewById(R.id.releaseDateFilmSearch);
        image = (ImageView) itemView.findViewById(R.id.imageFilmSearch);
        descriptionView = itemView.findViewById(R.id.descriptionFilmSearch);
        this.liste = liste;
    }

    public void display(Film film) {
        nameView.setText(film.getTitle());
        dateView.setText(film.getReleaseDate());

        if(film.getOverview() != null && !film.getOverview().isEmpty()) {
            descriptionView.setText(film.getOverview());
            descriptionView.append("...");
        }
        else {
            descriptionView.setText(R.string.no_description_fr);
        }

        if(film.getPosterPath() != null && !film.getPosterPath().isEmpty()) { //Si image
            Picasso.with(image.getContext()).load(imagePathBuilder(film.getPosterPath())).into(image);
        }
        else { //Si pas d'image, image par défaut
            Picasso.with(image.getContext()).load(R.drawable.image_not_found).into(image);
        }
    }

    public String imagePathBuilder(String path) {
        return "https://image.tmdb.org/t/p/w500/" + path;
    }

    @Override
    public void onClick(View v) {
        //Au click sur un item, on ouvre les détails du film
        clikedView = v;
        Film clicked = liste.get(getLayoutPosition());
        Intent details = new Intent(clikedView.getContext() , DetailsActivity.class);
        details.putExtra("FilmId", clicked.getId().toString());
        clikedView.getContext().startActivity(details);




    }
}

