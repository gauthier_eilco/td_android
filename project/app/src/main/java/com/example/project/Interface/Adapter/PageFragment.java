package com.example.project.Interface.Adapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.project.Model.Film;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.VideoPlayer;
import com.example.project.R;
import com.example.project.Service.RequeteMaker;

import java.io.IOException;


public class PageFragment extends Fragment {

    // 1 - Create keys for our Bundle
    private static final String KEY_POSITION="position";
    private  static final String ImgUrl="ImgUrl";
    private  static final String PosterUrl="PosterUrl";
    private  static final String VideoKEY="VideoKEY";





    public void PageFragment() { }


    // 2 - Method that will create a new instance of PageFragment, and add data to its bundle.
    public static PageFragment newInstance(int position, Film result) throws IOException {
        RequeteMaker requete = new RequeteMaker();

        // 2.1 Create new fragment
        PageFragment frag = new PageFragment();

        // 2.2 Create bundle and add it some data
        Bundle args = new Bundle();
        String posterUrl="";

        args.putInt(KEY_POSITION, position);
        String poster_path_formated = "https://image.tmdb.org/t/p/w780/"+result.getPosterPath()+"";
        /*poster poster = new poster(result.poster_path);*/
        args.putString(ImgUrl,poster_path_formated);
        ObjectVideo objectVideo = null;
        objectVideo = requete.gevideoUrl("FR", result.getId());
        String videoKey=objectVideo.getResults().get(1).getKey();


        posterUrl ="https://img.youtube.com/vi/"+videoKey+"/hqdefault.jpg";

        args.putString(VideoKEY,videoKey);
        args.putString(PosterUrl,posterUrl);


        //String FilmID=result.getId();


        frag.setArguments(args);

        return(frag);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        View view = inflater.inflate(R.layout.film, container, false);

        int position = getArguments().getInt(KEY_POSITION, -1);
        String Url = getArguments().getString(PosterUrl,"PosterUrl");
        final String VideoKey= getArguments().getString(VideoKEY,"VideoKEY");

        //TextView Titreview = (TextView)view.findViewById(R.id.Titre);
        ImageView image=(ImageView)view.findViewById(R.id.poster);

        image.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String clickedDataItem = VideoKey;
                Context context = v.getContext();
                Intent intent = new Intent(context, VideoPlayer.class);
                intent.putExtra("url",clickedDataItem);
                context.startActivity(intent);
            }
        });

        //Titreview.setText(Titretext);
        Glide.with(this).load(Url).diskCacheStrategy(DiskCacheStrategy.ALL).into(image);


        Log.e(getClass().getSimpleName(), "onCreateView called for fragment number "+position);

        return view;
    }

}
