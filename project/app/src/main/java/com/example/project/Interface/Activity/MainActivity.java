package com.example.project.Interface.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.project.Controller.TmdbClient;
import com.example.project.Interface.Adapter.GenreAdapter;
import com.example.project.Interface.Adapter.PageAdapter;
import com.example.project.Interface.Adapter.PosterAdapter;
import com.example.project.Interface.Adapter.SimilarListAdapter;
import com.example.project.Model.Film;
import com.example.project.Model.Genre;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.ResultSearch;
import com.example.project.Model.ResultsVideo;
import com.example.project.R;
import com.example.project.Service.RequeteMaker;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    private static final int DIALOG_ALERT = 10;
    public static Context context=null;
    public static ProgressBar spinner;
    List<Film> resultsList = new ArrayList<>();
    final List<Genre> genreList = new ArrayList<>();
    private static int nombregenres=0;
    private static Random random;
    private Spinner genreSelect;
    private Spinner favselect;
    private RecyclerView recyclerViewFilmGenre;
    private ProgressDialog dialog_loading;
    private final static String API_KEY="a870ad0110ff6bafa39b352d0dd503dd";
    private final static String language="FR";
    static int genreid;
    static String titre;
    static String Titrefav;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.activity_main_bottom_navigation);
        genreSelect = (Spinner) findViewById(R.id.genre_select);
        favselect =(Spinner) findViewById(R.id.fav_select);

        recyclerViewFilmGenre = findViewById(R.id.R2);

        getPopulaireFilms();
        dialog_loading = ProgressDialog.show(MainActivity.this, "",
                "Chargement des films en cours...", true);
        context = this;
        fillFavSelect();
        fillGenreSelect();
        dialog_loading.hide();

        this.configureBottomView();


        RequeteMaker Requete = new RequeteMaker();
        random = new Random();


    }

    public void fillGenreSelect() {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/genre/movie/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        TmdbClient Objetvideopop = retrofit.create(TmdbClient.class);
        Call<ResultSearch> call_genre = Objetvideopop.getgenresLanguageandPage("a870ad0110ff6bafa39b352d0dd503dd","FR",1);
        call_genre.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(final Call<ResultSearch> call_genre, Response<ResultSearch> response) {
                List<Genre> res = response.body().getGenres();

                res.add(0, new Genre(-1,  getResources().getString(R.string.choisir_genre)));

                genreSelect.setAdapter(new ArrayAdapter<Genre>(context, android.R.layout.simple_spinner_dropdown_item, res));
                final Genre[] selected = {(Genre) genreSelect.getAdapter().getItem(1)};
                final TextView titreview =findViewById(R.id.titregenre);
                titreview.setText(selected[0].getName());
                titre =selected[0].getName();
                genreid=selected[0].getId();
                lookUpForFilms(selected[0]);

                genreSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                         selected[0] = (Genre) genreSelect.getAdapter().getItem(position);
                        if(!selected[0].getName().equals(getResources().getString(R.string.choisir_genre))) {
                            dialog_loading = ProgressDialog.show(MainActivity.this, "",
                                    "Chargement des films en cours...", true);
                            //Implémenter recherche de film selon le genre choisi
                            titreview.setText(selected[0].getName());
                            lookUpForFilms(selected[0]);
                            titre =selected[0].getName();
                            genreid=selected[0].getId();
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<ResultSearch> call_genre, Throwable t) {

            }
        });
    }
    public void fillFavSelect() {


        List<String> fav = new ArrayList<>();
        fav.add("Populaire");
        fav.add("En ce moment");
        fav.add("Les mieux notés");
        fav.add("Bientôt");

        favselect.setAdapter(new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, fav));
        final String[] selected = {(String) favselect.getAdapter().getItem(1)};
        final TextView titreview =findViewById(R.id.titrefav);
        titreview.setText(selected[0]);
        Retrofit.Builder builderfav = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofitfav = builderfav.build();
        final TmdbClient Objetfilmfav = retrofitfav.create(TmdbClient.class);

        final Call<ResultSearch>[] call_fav = new Call[]{Objetfilmfav.getfilmpopLanguageandPage(API_KEY, language, 1)};

        call_fav[0] = Objetfilmfav.getfilmpopLanguageandPage(API_KEY, language, 1);
        titreview.setText("Films Populaires");


        favselect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.transparent));
                        ((TextView) parent.getChildAt(0)).setTextSize(0);
                        selected[0] = (String) favselect.getAdapter().getItem(position);

                        if(!selected[0].equals(getResources().getString(R.string.choisir_categorie)))
                        {
                            dialog_loading = ProgressDialog.show(MainActivity.this, "",
                                    "Chargement des films en cours...", true);
                            //Implémenter recherche de film selon le genre choisi
                            titreview.setText(selected[0]);
                            Titrefav =selected[0];

                            lookUpForFilmsFav(call_fav[0]);
                        }
                        if ("Populaire".equals(selected[0]))
                        {
                            call_fav[0] = Objetfilmfav.getfilmpopLanguageandPage(API_KEY, language, 1);
                            titreview.setText("Films Populaires");
                            Titrefav ="Films Populaires";
                        } else if ("En ce moment".equals(selected[0]))
                        {
                            call_fav[0] = Objetfilmfav.getnow_playingLanguageandPage(API_KEY, language, 1);
                            titreview.setText("En ce moment");
                            Titrefav ="En ce moment";
                        } else if ("Les mieux notés".equals(selected[0]))
                        {
                            call_fav[0] = Objetfilmfav.gettop_ratedLanguageandPage(API_KEY, language, 1);
                            titreview.setText("Les mieux notés");
                            Titrefav ="Les mieux notés";
                        } else if ("Bientôt".equals(selected[0]))
                        {
                            call_fav[0] = Objetfilmfav.getupcomingLanguageandPage(API_KEY, language, 1);
                            titreview.setText("Films à venir");
                            Titrefav ="Bientôt";
                        } else
                            {
                            call_fav[0] = Objetfilmfav.getupcomingLanguageandPage(API_KEY, language, 1);
                                Titrefav ="Films Populaires";
                        }





                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
    public void lookUpForFilmsFav(Call<ResultSearch> call_fav) {
        call_fav.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(Call<ResultSearch> call_fav, Response<ResultSearch> response_fav) {
                List<Film> films = response_fav.body().getListFilms();
                final List<String> listetitres = new ArrayList<>();
                final List<String> listeposter_url = new ArrayList<>();
                final List<String> listevideoKeys = new ArrayList<>();
                final List<Integer> listeFilmId = new ArrayList<>();
                assert response_fav.body() != null;
                for (final Film film : films) {

                    final String titre = film.getTitle();
                    final Integer FilmId =film.getId();
                    final String poster_url = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + film.getPosterPath();
                    int movie_id = film.getId();

                    Retrofit.Builder buildervideo = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/" + movie_id + "/").addConverterFactory(GsonConverterFactory.create());
                    Retrofit retrofitvideo = buildervideo.build();
                    TmdbClient ObjetVideo = retrofitvideo.create(TmdbClient.class);
                    Call<ObjectVideo> callvideo = ObjetVideo.getLanguage(API_KEY, language);
                    callvideo.enqueue(new Callback<ObjectVideo>() {
                        @Override
                        public void onResponse(Call<ObjectVideo> callvideo, Response<ObjectVideo> responsevideo) {

                            for (ResultsVideo VideoID : responsevideo.body().getResults()) {
                                if (VideoID.getSite().equals("YouTube")) {
                                    listevideoKeys.add(VideoID.getKey());
                                    listetitres.add(titre);
                                    listeposter_url.add(poster_url);
                                    listeFilmId.add(FilmId);


                                    PosterAdapter posterAdapter = new PosterAdapter(listeposter_url,listevideoKeys,listetitres,listeFilmId,1);
                                    RecyclerView recyclerfilmfav = findViewById(R.id.R1);
                                    recyclerfilmfav.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    recyclerfilmfav.setAdapter(posterAdapter);
                                    break;
                                } else {
                                    continue;
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<ObjectVideo> call, Throwable t)
                        {
                            Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                dialog_loading.hide();

                Log.d("sizez", String.valueOf(films.size()));


            }

            @Override
            public void onFailure(Call<ResultSearch> call, Throwable t) {

            }
        });

        //dialog_loading.hide();


    }


    public void lookUpForFilms(Genre genre) {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        TmdbClient client = retrofit.create(TmdbClient.class);
        Call<ResultSearch> call = client.getFilmsByGenre("a870ad0110ff6bafa39b352d0dd503dd","FR", genre.getId().toString());
        call.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(Call<ResultSearch> call, Response<ResultSearch> response) {
                List<Film> films = response.body().getListFilms();
                final List<String> listetitres = new ArrayList<>();
                final List<String> listeposter_url = new ArrayList<>();
                final List<String> listevideoKeys = new ArrayList<>();
                final List<Integer> listeFilmId = new ArrayList<>();

                assert response.body() != null;
                for (final Film film : films) {

                    final String titre = film.getTitle();
                    final Integer FilmId = film.getId();
                    final String poster_url = "https://image.tmdb.org/t/p/w300_and_h450_bestv2/" + film.getPosterPath();
                    int movie_id = film.getId();

                    Retrofit.Builder buildervideo = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/" + movie_id + "/").addConverterFactory(GsonConverterFactory.create());
                    Retrofit retrofitvideo = buildervideo.build();
                    TmdbClient ObjetVideo = retrofitvideo.create(TmdbClient.class);
                    Call<ObjectVideo> callvideo = ObjetVideo.getLanguage(API_KEY, language);
                    callvideo.enqueue(new Callback<ObjectVideo>() {
                        @Override
                        public void onResponse(Call<ObjectVideo> callvideo, Response<ObjectVideo> responsevideo) {

                            for (ResultsVideo VideoID : responsevideo.body().getResults()) {
                                if (VideoID.getSite().equals("YouTube")) {
                                    listevideoKeys.add(VideoID.getKey());
                                    listetitres.add(titre);
                                    listeposter_url.add(poster_url);
                                    listeFilmId.add(FilmId);


                                    PosterAdapter posterAdapter = new PosterAdapter(listeposter_url,listevideoKeys,listetitres,listeFilmId,1);
                                    RecyclerView recyclerfilmfav = findViewById(R.id.R2);
                                    recyclerfilmfav.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false));
                                    recyclerfilmfav.setAdapter(posterAdapter);
                                    break;
                                } else {
                                    continue;
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<ObjectVideo> call, Throwable t)
                        {
                            Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                dialog_loading.hide();

                Log.d("sizez", String.valueOf(films.size()));


            }

            @Override
            public void onFailure(Call<ResultSearch> call, Throwable t) {

            }
        });
        //dialog_loading.hide();
    }

    public void getPopulaireFilms() {
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        TmdbClient Objetvideopop = retrofit.create(TmdbClient.class);
        Call<ResultSearch> call = Objetvideopop.getfilmpopLanguageandPage("a870ad0110ff6bafa39b352d0dd503dd","FR",1);

        call.enqueue(new Callback<ResultSearch>() {
            @Override
            public void onResponse(Call<ResultSearch> call, Response<ResultSearch> response)
            {
                for (int i = 0; i < 6; i++) {
                    resultsList.add(response.body().getListFilms().get(i));
                }
                ViewPager pager = findViewById(R.id.activity_main_viewpager);
                pager.setAdapter(new PageAdapter(getSupportFragmentManager(), resultsList));
            }

            @Override
            public void onFailure(Call<ResultSearch> call, Throwable t) {

            }
        });

    }





    public static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    private void configureBottomView(){
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                Intent intent = null;
                switch (menuItem.getItemId()) {
                    case R.id.action_menu_accueil:
                        intent = new Intent(MainActivity.this.getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_search:
                        intent = new Intent(MainActivity.this.getApplicationContext(), SearchActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_menu_settings:
                        showDialog(DIALOG_ALERT);
                        break;
                }
                return true;
            }
        });

        bottomNavigationView.getMenu().findItem(R.id.action_menu_accueil).setChecked(true);
    }




    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_ALERT:
                // Create out AlterDialog
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_DARK);
                builder.setTitle("MoviMee");
                builder.setMessage(Html.fromHtml("<p>L'application MoviMee a été dévelopée en 2019 par <b>Gauthier Vangrevelynghe</b> et <b>Jori Mathurin</b> dans le cadre d'un projet d'école.</p><p>Elle vous permet de facilement naviguer au sein d'une large collection de films populaires et d'en obtenir les détails.</p>"));
                builder.setCancelable(true);
                builder.setNeutralButton("OK", new MainActivity.OkOnClickListener());
                AlertDialog dialog = builder.create();
                dialog.show();
        }
        return super.onCreateDialog(id);
    }

    private final class OkOnClickListener implements
            DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialog, int which) {
        }
    }


    public void plusfav(View v)
    {
        Context context = v.getContext();
        Intent intent = new Intent(context,Grille.class);
        intent.putExtra("Titrefav", Titrefav);
        context.startActivity(intent);
    }

    public void plusgenre(View v) {
        Context context = v.getContext();
        Intent intent = new Intent(context,Grille.class);
        intent.putExtra("genreid",genreid);
        intent.putExtra("titre",titre);
        context.startActivity(intent);
    }






}
