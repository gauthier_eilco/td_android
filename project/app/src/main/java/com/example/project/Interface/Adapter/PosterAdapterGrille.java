package com.example.project.Interface.Adapter;

import android.annotation.SuppressLint;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.project.Interface.Activity.DetailsActivity;
import com.example.project.Interface.Activity.Grille;
import com.example.project.Interface.Activity.MainActivity;
import com.example.project.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class PosterAdapterGrille extends RecyclerView.Adapter<PosterAdapterGrille.PosterViewHolder>{
    private List<String>images= new ArrayList<>();
    private List<String>videoID=new ArrayList<>();
    private List<String> Titres = new ArrayList<>();
    private List<Integer> FilmId = new ArrayList<>();
    private static int from;


    static Context context;

    public PosterAdapterGrille(List<String> poster, List<String> videoUrl, List<String> Titres,List<Integer> FilmId,int from)
    {
        this.images=poster;
        this.videoID=videoUrl;
        this.Titres=Titres;
        this.FilmId=FilmId;
        this.from=from;


    }


    @NonNull
    @Override
    public PosterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.poster,parent,false);
        context = parent.getContext();


        return new PosterAdapterGrille.PosterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PosterViewHolder holder, final int position)
    {
        holder.itemView.setTag(position);

        Glide.with(holder.itemView).load(images.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.getImage());



        holder.getImage().setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {

                final LayoutInflater fact =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                @SuppressLint("InflateParams") final View view = fact.inflate(R.layout.popup,null);
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                TextView Titre = view.findViewById(R.id.Titrepopup);
                ImageView Poster = view.findViewById(R.id.posterpopup);
                ImageView Youtubeicon = view.findViewById(R.id.player);
                Titre.setText(Titres.get(position));


                    Glide.with(Grille.context).load(images.get(position)).diskCacheStrategy(DiskCacheStrategy.ALL).into(Poster);

                builder.setView(view);
                builder.setCancelable(true);

                Youtubeicon.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        String clickedDataItem =  videoID.get(position);
                        Context context = v.getContext();
                        Intent intent = new Intent(context,com.example.project.Model.VideoPlayer.class);
                        intent.putExtra("url",clickedDataItem);
                        context.startActivity(intent);
                    }
                });

                Poster.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        int clickedDataItem =  FilmId.get(position);
                        Context context = v.getContext();
                        Intent intent = new Intent(context, DetailsActivity.class);
                        intent.putExtra("FilmId",String.valueOf(clickedDataItem));
                        context.startActivity(intent);
                    }
                });


                final AlertDialog alert =builder.create();

                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                alert.show();


            }
        })
        ;
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    class PosterViewHolder extends RecyclerView.ViewHolder{

        ImageView Im ;

        PosterViewHolder(@NonNull View itemView) {



            super(itemView);
            Im=(ImageView)itemView.findViewById(R.id.imageposter);
        }
        private ImageView getImage(){return Im;}
    }
}