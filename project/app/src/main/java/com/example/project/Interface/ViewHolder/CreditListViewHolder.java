package com.example.project.Interface.ViewHolder;

import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Interface.Activity.DetailsActivity;
import com.example.project.Model.Cast;
import com.example.project.Model.Film;
import com.example.project.R;
import com.example.project.Service.DownloaderService;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CreditListViewHolder extends RecyclerView.ViewHolder {
    private TextView nameView, characterView;
    private ImageView image;

    public CreditListViewHolder(View itemView, List<Cast> casts) {
        super(itemView);
        image = itemView.findViewById(R.id.imageCast);
        nameView = itemView.findViewById(R.id.name);
        characterView = itemView.findViewById(R.id.character);
    }

    public void display(Cast cast) {
        nameView.setText(Html.fromHtml("<u>" + cast.getName()+ "</u>"));
        characterView.setText(cast.getCharacter());

        if(cast.getProfilePath() != null && !cast.getProfilePath().isEmpty()) { //Si image
            new DownloaderService(image.getContext(), cast.getProfilePath(), image, 400,600);
        }
        else { //Si pas d'image, image par défaut
            new DownloaderService(image.getContext(), R.drawable.image_not_found, image, 400,600);
        }
    }

}
