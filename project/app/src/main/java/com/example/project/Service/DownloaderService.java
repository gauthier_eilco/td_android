package com.example.project.Service;

import android.content.Context;
import android.media.Image;
import android.widget.ImageView;

import com.example.project.R;
import com.squareup.picasso.Picasso;

public class DownloaderService {
    Context context;
    String path;
    ImageView img;
    int drawable;
    int w, h;


    public DownloaderService(Context c, String p, ImageView img) {
        this.context = c;
        this.path = imagePathBuilder(p);
        this.img = img;

        load();
    }

    public DownloaderService(Context c, int drawable, ImageView img) {
        this.context = c;
        this.drawable = drawable;
        this.img = img;

        loadWithDrawable();
    }

    public DownloaderService(Context c, String p, ImageView img, int w, int h) {
        this.context = c;
        this.path = imagePathBuilder(p);
        this.img = img;
        this.w = w;
        this.h = h;

        loadAndResize();
    }

    public DownloaderService(Context c, int drawable, ImageView img, int w, int h) {
        this.context = c;
        this.drawable = drawable;
        this.img = img;
        this.w = w;
        this.h = h;

        loadWithDrawableAndResize();
    }

    public void load() {
        Picasso.with(context)
                .load(path)
                .into(img);
    }

    public void loadWithDrawable() {
        Picasso.with(context)
                .load(drawable)
                .into(img);
    }

    public void loadAndResize() {
        Picasso.with(context)
                .load(path)
                .resize(w,h)
                .into(img);
    }

    public void loadWithDrawableAndResize() {
        Picasso.with(context)
                .load(drawable)
                .resize(w,h)
                .into(img);
    }


    public String imagePathBuilder(String path) {
        return "https://image.tmdb.org/t/p/w500/" + path;
    }
}
