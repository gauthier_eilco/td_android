package com.example.project.Model;

import java.util.List;

public class ResultSearch {
    private int id;
    private List<Film> results = null;
    private int total_results;
    private int total_pages;
    private  List<Genre> genres;

    public int getId() {
        return id;
    }

    public List<Film> getResults() {
        return results;
    }

    public void setResults(List<Film> results) {
        this.results = results;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Film> getListFilms() {
        return results;
    }

    public int getTotalResults() { return total_results; }

    public int getTotalPages() { return total_pages; }
}
