package com.example.project.Interface.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

import com.example.project.Interface.ViewHolder.CreditListViewHolder;
import com.example.project.Interface.ViewHolder.FilmListViewHolder;
import com.example.project.Interface.ViewHolder.SimilarListViewHolder;
import com.example.project.Model.Cast;
import com.example.project.Model.Film;
import com.example.project.Model.ResultSearch;
import com.example.project.R;

public class SimilarListAdapter extends RecyclerView.Adapter<SimilarListViewHolder>{

    private List<Film> similars;

    public SimilarListAdapter(List<Film> filmToDisplay) {this.similars = filmToDisplay;}

    @Override
    public SimilarListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_film_similar, parent, false);
        return new SimilarListViewHolder(view, similars);
    }

    @Override
    public void onBindViewHolder(SimilarListViewHolder holder, int position) {
        Film to_display = similars.get(position);
        if(to_display.getPosterPath() != null && !to_display.getPosterPath().equals(""))
            holder.display(similars.get(position));
    }

    @Override
    public int getItemCount() {
        return similars.size();
    }
}
