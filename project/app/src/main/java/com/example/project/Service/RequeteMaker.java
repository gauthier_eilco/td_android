package com.example.project.Service;
import android.os.StrictMode;

import com.example.project.Controller.TmdbClient;
import com.example.project.Model.ObjectVideo;
import com.example.project.Model.ResultSearch;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RequeteMaker
{

    private final static String API_KEY="a870ad0110ff6bafa39b352d0dd503dd";
    private Retrofit.Builder builder = null;

    public RequeteMaker()
    {}

    public ResultSearch getpop(String language, int page) throws IOException
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Retrofit.Builder builder = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        TmdbClient Objetvideopop = retrofit.create(TmdbClient.class);
        Call<ResultSearch> call = Objetvideopop.getfilmpopLanguageandPage(API_KEY,language,page);
        return call.execute().body();
    }

    public ResultSearch getgenre(String language, int page) throws IOException
{
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
    StrictMode.setThreadPolicy(policy);
    Retrofit.Builder buildergenre = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/genre/movie/").addConverterFactory(GsonConverterFactory.create());
    Retrofit retrofitgenre = buildergenre.build();
    TmdbClient ObjetGenre = retrofitgenre.create(TmdbClient.class);
    Call<ResultSearch> callgenre = ObjetGenre.getgenresLanguageandPage(API_KEY,language,page);
    return callgenre.execute().body();

}
    public ResultSearch getfilmsbygenre(String language, int page,int genre) throws IOException
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Retrofit.Builder buildergenre = new Retrofit.Builder().baseUrl("https:///api.themoviedb.org/3/discover/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofitgenre = buildergenre.build();
        TmdbClient ObjetfilmsGenre = retrofitgenre.create(TmdbClient.class);
        Call<ResultSearch> callgenre = ObjetfilmsGenre.getfilmsgenresLanguageandPage(API_KEY,genre,language,page);
        return callgenre.execute().body();

    }

    public ObjectVideo gevideoUrl(String language,int movie_id) throws IOException
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Retrofit.Builder buildergenre = new Retrofit.Builder().baseUrl("https://api.themoviedb.org/3/movie/"+movie_id+"/").addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofitvideo= buildergenre.build();
        TmdbClient ObjetVideo = retrofitvideo.create(TmdbClient.class);
        Call<ObjectVideo> callvideo = ObjetVideo.getLanguage(API_KEY,language);
        ObjectVideo video = callvideo.execute().body();


        return video;
        }



    }


